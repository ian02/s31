const express = require ('express');
const router = express.Router ();

const taskController = require ('../controllers/taskController');

//Routes to get all tasks
router.get ('/', (req, res) => {
	taskController.getAllTasks().then (resultFromController => res.send(resultFromController));
})


//Route to post or add a task /tasks
router.post ('/', (req, res) => {
	taskController.createTask (req.body).then(resultFromController => res.send (resultFromController))
})

//Route to get specific id/record
router.get ('/:id', (req, res) => {
	// res.send (req.params.id)
	taskController.getTask(req.params.id)
	//getTask(23)
	//getTask(taskId)
	// returned task with Id 23
	.then(resultFromController => res.send (resultFromController))
})

/*
//Sir Alan's notes
// Update Task
// Route to update a task
// This route expects to receive a PUT request at the URL "/tasks/:id"
// The whole URL is at "http://localhost:3001/tasks/:id"
router.put("/:id", (req, res) => {
// The "updateTask" function will accept the following 2 arguments:
        // "req.params.id" retrieves the task ID from the parameter
        // "req.body" retrieves the data of the updates that will be applied to a task from the request's "body" property
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

})
*/

//Update a Specific Task
    //http://localhost:3001/tasks/id
router.put("/:id", (req, res) => {
    // console.log(req.params)
    // console.log(req.params.id)
    let id = req.params.id;
    let newContent = req.body

    taskController.updateTask(id, newContent).then( (result) => {
        res.send(result)
    })
})


router.delete ('/delete/:id', (req, res) => {
	taskController.deleteTask(req.params.id).then (resultFromController => res.send (resultFromController))
})

//Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;
















