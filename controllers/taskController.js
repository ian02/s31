const Task = require ('../models/task')

//Get all tasks
module.exports.getAllTasks = () => {
	return Task.find ({}).then (result => {
		return result;
	})
}

//Add a task
module.exports.createTask = (requestBody) => {
	let newTask = new Task ({
		name: requestBody.name
	})
	return newTask.save().then((task, error) => {
		if (error){
			console.log (error)
		} else {
			return task;
		}
	})
}

//Get single task/record
//Get the id from the URL
//Use that id to find it in the tasks collection
//Use Task model. Task.findById()
module.exports.getTask = (taskId) => {
	//The "findById" Mongoose method will look for a task with the same id provided from the URL
	/*
	Task.findById(23).then ((result, error) => {
	if (error){
		console.log (error)
		return false
	} else {
		return result
	}
	})
	*/
	return Task.findById (taskId).then((result, error) => {
		if (error){
			console.log (error)
			return false;
		//Find successful, returns the task object back to the client/Postman
		} else {
			return result;
		}
	})
}

//Updating a task
module.exports.updateTask = (id, newContent) => {
    // console.log(newContent)
    // console.log(id)
    //find the document
        //Model.findById()
    return Task.findById(id).then( (result, error) => {
        console.log(result)

        if(result == null){
            return 'document not existing'
        }

        if(error){
            console.log(error)
            return false
        }

        //Update the retrieved document with a value coming from the client (req.body)
        // console.log(result)
        result.name = newContent.name

        //after updating the document, we will  use save() method to update the document in the database
        return result.save().then( (updatedTask, error) => {
            if(error){
                return false
            } else {
                return updatedTask
            }
        })
    })
}




module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove (taskId).then ((removeTask, err) => {
		if (err) {
			console.log (err)
			return false;
		} else {
			return removeTask
		}
	})
}

/*
//Sir Alan's notes
// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"
module.exports.updateTask = (taskId, newContent) => {

    // The "findById" Mongoose method will look for a task with the same id provided from the URL
    // "findById" is the same as "find({"_id" : value})"
    // The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function
    return Task.findById(taskId).then((result, error) => {

        // If an error is encountered returns a "false" boolean back to the client/Postman
        if(error){

            console.log(err);
            return false;

        }

        // Results of the "findById" method will be stored in the "result" parameter
        // It's "name" property will be reassigned the value of the "name" received from the request
        result.name = newContent.name;

        // Saves the updated object in the MongoDB database
        // The document already exists in the database and was stored in the "result" parameter
        // Because of Mongoose we have access to the "save" method to update the existing document with the changes we applied
        // The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method which invokes this function
        return result.save().then((updatedTask, saveErr) => {

            // If an error is encountered returns a "false" boolean back to the client/Postman
            if (saveErr){

                // The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
                console.log(saveErr);
                return false;

            // Update successful, returns the updated task object back to the client/Postman
            } else {

                /// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
                return updatedTask;

            }
        })
    })
}

*/







