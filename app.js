const express = require ('express');
const mongoose = require ('mongoose');


//This allows us to use the routes defined in "taskRoute.js"
const taskRoute = require('./routes/taskRoute');

const app = express ();
const port = 3001;

app.use (express.json());
app.use (express.urlencoded ({extended:true}));

//Connect to MongoBD
mongoose.connect ('mongodb+srv://admin:admin1234@zuitt-bootcamp.ktm2j.mongodb.net/s31?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

//Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
//to use "/tasks" route
app.use('/tasks', taskRoute)


//Server listening
app.listen (port, () => {
	console.log (`Listening to port ${port}`)
});